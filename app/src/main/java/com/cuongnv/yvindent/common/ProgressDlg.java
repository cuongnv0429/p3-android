/**
 * ProgressDlg
 * A class show progress dialog when loading something in background
 *
 * @author Briswell - Nguyen Van Tu
 * @version 1.0 2016-07-13
 */


package com.cuongnv.yvindent.common;


import android.app.ProgressDialog;
import android.content.Context;


public class ProgressDlg {

    private ProgressDialog progressDialog;


    public ProgressDlg(Context context) {
        if (null == context) {
            return;
        }
        progressDialog = new ProgressDialog(context);
        progressDialog.setCanceledOnTouchOutside(false);
        //progressDialog.setContentView(R.layout.dialog_custom_progress);
    }

    /**
     * Show Progress Dialog on top screen with message
     */
    public void show() {
        //progressDialog.setMessage("");
        progressDialog.show();

    }

    /**
     * Show Progress Dialog on top screen with message
     */
    public void show(String message) {
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    /**
     * Hide Progress Dialog, if it is showing.
     */
    public void hide() {
        progressDialog.dismiss();
    }

    /**
     * Check if Progress Dialog is showing.
     */
    public boolean isShowing() {
        return progressDialog.isShowing();
    }
}
