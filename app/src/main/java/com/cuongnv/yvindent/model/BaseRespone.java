package com.cuongnv.yvindent.model;

import java.util.List;

/**
 * Created by ITV01 on 1/15/17.
 */

public class BaseRespone {
    private int status;
    private List<Post> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<Post> getData() {
        return data;
    }

    public void setData(List<Post> data) {
        this.data = data;
    }
}
