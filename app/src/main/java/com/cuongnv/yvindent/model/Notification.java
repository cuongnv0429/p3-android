package com.cuongnv.yvindent.model;

/**
 * Created by ITV01 on 4/6/17.
 */

public class Notification {
    private String id;
    private String name;
    private String detail;
    private String date;

    public Notification() {

    }

    public Notification(String id, String name, String detail, String date) {
        this.id = id;
        this.name = name;
        this.detail = detail;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
