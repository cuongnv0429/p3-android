package com.cuongnv.yvindent.model;

/**
 * Created by ITV01 on 1/15/17.
 */

public class Category {
    private String nameCategory;
    private int idImages;

    public Category(int id, String nameCategory) {
        this.idImages = id;
        this.nameCategory = nameCategory;
    }

    public int getIdImages() {
        return idImages;
    }

    public void setIdImages(int idImages) {
        this.idImages = idImages;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }
}
