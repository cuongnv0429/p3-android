package com.cuongnv.yvindent.model;

/**
 * Created by ITV01 on 2/18/17.
 */

public class UpdateInforRequest {
    private String email;
    private String phone;
    private String address;
    private String gender;
    private String dob;
    private String fullname;

    public UpdateInforRequest() {
        this.email = "";
        this.phone = "";
        this.address = "";
        this.gender = "";
        this.dob = "";
        this.fullname = "";
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }
}
