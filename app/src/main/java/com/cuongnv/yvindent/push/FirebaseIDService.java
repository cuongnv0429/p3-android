package com.cuongnv.yvindent.push;

import android.content.SharedPreferences;

import com.cuongnv.yvindent.utils.Constant;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by ITV01 on 2/8/17.
 */

public class FirebaseIDService extends FirebaseInstanceIdService{
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        SharedPreferences mySharedPreferences = getSharedPreferences("yvindent", MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor = mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.PUSH_TOKEN, FirebaseInstanceId.getInstance().getToken());
        sharedpreferenceeditor.commit();
    }
}
