package com.cuongnv.yvindent.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.databinding.ActivityViewAdsBinding;

public class ViewAdsActivity extends AppCompatActivity {

    private String url = "";
    ActivityViewAdsBinding activityViewAdsBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityViewAdsBinding = DataBindingUtil.setContentView(this, R.layout.activity_view_ads);
        url = getIntent().getStringExtra("url");
        activityViewAdsBinding.webView.getSettings().setJavaScriptEnabled(true);
        activityViewAdsBinding.webView.loadUrl(url);
    }
}
