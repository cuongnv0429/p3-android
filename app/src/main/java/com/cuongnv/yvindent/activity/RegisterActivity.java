package com.cuongnv.yvindent.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Window;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.fragment.FragmentLogin;
import com.cuongnv.yvindent.model.UserInfor;
import com.cuongnv.yvindent.utils.BaseActivity;
import com.cuongnv.yvindent.utils.BaseFragment;
import com.google.gson.JsonObject;

public class RegisterActivity extends BaseActivity {
    private BaseActivity baseActivity = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_register);
        if (!baseActivity.getUserId(baseActivity).equalsIgnoreCase("")) {
            showActivity(MainActivity.class);
        } else {
            showFragment(new FragmentLogin());
        }
    }

    public void showFragment(BaseFragment baseFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_register, baseFragment)
                .addToBackStack(null)
                .commit();
    }

    public void showActivity(Class toClass) {
        Intent intent = new Intent(this, toClass);
        startActivity(intent);
        finish();
    }

    public UserInfor parseJsonUserInfor(JsonObject userObject) {
        if (userObject != null) {
            UserInfor userInfor = new UserInfor();
            if (userObject.has("id")) {
                userInfor.setId(userObject.get("id").getAsString());
            }
            if (userObject.has("username")) {
                userInfor.setUsername(userObject.get("username").getAsString());
            }
            if (userObject.has("email")) {
                userInfor.setEmail(userObject.get("email").getAsString());
            }
            if (userObject.has("phone")) {
                try {
                    userInfor.setPhone(userObject.get("phone").getAsString());
                } catch (UnsupportedOperationException un) {
                    un.printStackTrace();
                }
            }
            if (userObject.has("address")) {
                try {
                    userInfor.setAddress(userObject.get("address").getAsString());
                } catch (UnsupportedOperationException un) {
                    un.printStackTrace();
                }
            }
            if (userObject.has("status")) {
                userInfor.setStatus(userObject.get("status").getAsString());
            }
            if (userObject.has("level")) {
                userInfor.setLevel(userObject.get("level").getAsString());
            }
            if (userObject.has("fullname")) {
                try {
                    userInfor.setFullname(userObject.get("fullname").getAsString());
                } catch (UnsupportedOperationException un) {
                    un.printStackTrace();
                }
            }
            if (userObject.has("token")) {
                try {
                    userInfor.setToken(userObject.get("token").getAsString());
                } catch (UnsupportedOperationException un) {
                    un.printStackTrace();
                }
            }
            if (userObject.has("dob")) {
                try {
                    userInfor.setDob(userObject.get("dob").getAsString());
                } catch (UnsupportedOperationException un) {
                    un.printStackTrace();
                }
            }
            if (userObject.has("gender")) {
                try {
                    userInfor.setGender(userObject.get("gender").getAsString());
                } catch (UnsupportedOperationException un) {
                    un.printStackTrace();
                }
            }
            if (userObject.has("point")) {
                try{
                    userInfor.setPoint(userObject.get("point").getAsInt());
                } catch (UnsupportedOperationException un) {
                    un.printStackTrace();
                }
            }
            if (userObject.has("total_point")) {
                try {
                    userInfor.setTotal_point(userObject.get("total_point").getAsInt());
                } catch (UnsupportedOperationException un) {
                    un.printStackTrace();
                }
            }
            if (userObject.has("totalPoint")) {
                try {
                    userInfor.setTotalPoint(userObject.get("totalPoint").getAsInt());
                } catch (UnsupportedOperationException un) {
                    un.printStackTrace();
                }
            }
            return userInfor;
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 1) {
            fragmentManager.popBackStack();
        } else {
            finish();
        }
    }
}
