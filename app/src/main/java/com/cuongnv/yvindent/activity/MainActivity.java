package com.cuongnv.yvindent.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.adapter.CustomDrawerAdapter;
import com.cuongnv.yvindent.databinding.ActivityMainBinding;
import com.cuongnv.yvindent.fragment.FragmentListPost;
import com.cuongnv.yvindent.model.Category;
import com.cuongnv.yvindent.utils.BaseActivity;
import com.cuongnv.yvindent.utils.BaseFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements View.OnClickListener{

    public static final int REQUEST_CODE_SIGN_OUT = 111;
    public static final int RESULT_CODE_SIGN_OUT = 112;
    private String TAG = "MainActivity";
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    CustomDrawerAdapter adapter;
    ActivityMainBinding mainBinding;
    List<Category> dataList;
    private BaseFragment baseFragment;
    private int positionCategory = 0;
    private BaseActivity baseActivity = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        initView();
        mainBinding.leftDrawer.setOnItemClickListener(new DrawerItemClickListener());
    }

    private void initView() {
        mainBinding.toolBar.imbMenu.setOnClickListener(this);
        mainBinding.toolBar.txtTitle.setOnClickListener(this);
        mainBinding.toolBar.imbSearch.setOnClickListener(this);
        mainBinding.toolBar.txtLogout.setOnClickListener(this);
        mainBinding.toolBar.imbSetting.setOnClickListener(this);
        // Initializing

        mTitle = mDrawerTitle = getTitle();
//        mainBinding.drawerLayout.setDrawerShadow(R.drawable.icon_menu,
//                GravityCompat.START);
        //init data
        dataList = new ArrayList<Category>();
        dataList.add(new Category(0, "Tất cả"));
        dataList.add(new Category(R.drawable.forty_nine_p, "49P news"));
        dataList.add(new Category(R.drawable.ctcn, "Chuyên trang chỉnh nha"));
        dataList.add(new Category(R.drawable.ctph, "Chuyên trang phục hình"));
        adapter = new CustomDrawerAdapter(this, R.layout.custom_drawer_item,
                dataList);
        mainBinding.leftDrawer.setAdapter(adapter);
        pushFragment(baseFragment = new FragmentListPost());
    }

    private void selectCatergory(int position) {
        mainBinding.leftDrawer.setItemChecked(position, true);
//        setTitle(dataList.get(position).getNameCategory());
        mainBinding.drawerLayout.closeDrawer(mainBinding.leftDrawer);
        if (baseFragment instanceof FragmentListPost) {
            ((FragmentListPost) baseFragment).resetData(position + "");
        }
    }

    private void pushFragment(BaseFragment baseFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_main, baseFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        Log.e(TAG, getFragmentManager().getBackStackEntryCount() + "__");
        if (getFragmentManager().getBackStackEntryCount() > 1) {

        } else {
            if(mainBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                mainBinding.drawerLayout.closeDrawer(mainBinding.leftDrawer);
            } else {
                finish();
            }
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        mainBinding.toolBar.txtTitle.setText(mTitle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imb_menu:
                if(mainBinding.drawerLayout.isDrawerOpen(GravityCompat.START))
                    mainBinding.drawerLayout.closeDrawer(mainBinding.leftDrawer);
                else
                    mainBinding.drawerLayout.openDrawer(mainBinding.leftDrawer);
                break;
            case R.id.imb_search:
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                intent.putExtra("cat", positionCategory + "'");
                startActivity(intent);
                break;
            case R.id.txt_logout:
                Log.e(TAG, "txt_logout");
                baseActivity.saveIsLogin(baseActivity, false);
                baseActivity.saveToken(baseActivity, "");
                baseActivity.saveUserId(baseActivity, "");
                Intent intent1 = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent1);
                finish();
                break;
            case R.id.imb_setting:
                Intent intentSetting = new Intent(MainActivity.this, SettingActivity.class);
                startActivityForResult(intentSetting, REQUEST_CODE_SIGN_OUT);
                break;
        }
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            positionCategory = position;
            selectCatergory(position);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SIGN_OUT && resultCode == RESULT_CODE_SIGN_OUT) {
            Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
