package com.cuongnv.yvindent.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.databinding.ActivityPopupPostBuyBinding;
import com.cuongnv.yvindent.utils.BaseActivity;

public class PopupPostBuyActivity extends BaseActivity implements View.OnClickListener{

    ActivityPopupPostBuyBinding activityPopupPostBuyBinding;
    Intent intent;
    private int point = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityPopupPostBuyBinding = DataBindingUtil.setContentView(this, R.layout.activity_popup_post_buy);
        intent = getIntent();
        initView();
        if (intent != null) {
            point = intent.getExtras().getInt("point");
        }
        activityPopupPostBuyBinding.txtPostBuy.setText("Mua bài báo này - " + point + " điểm");
    }

    private void  initView() {
        activityPopupPostBuyBinding.txtPostBuy.setOnClickListener(this);
        activityPopupPostBuyBinding.txtPostNormal.setOnClickListener(this);
        activityPopupPostBuyBinding.txtPostCtcnBuy.setOnClickListener(this);
        activityPopupPostBuyBinding.txtPostCtphBuy.setOnClickListener(this);
        activityPopupPostBuyBinding.linearCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_post_buy:
                intent.putExtra("buy_code", 0);
                setResult(ReadPdfActivity.RESULT_CODE_READ_PDF, intent);
                finish();
                break;
            case R.id.txt_post_normal:
                intent.putExtra("buy_code", 1);
                setResult(ReadPdfActivity.RESULT_CODE_READ_PDF, intent);
                finish();
                break;
            case R.id.txt_post_ctcn_buy:
                intent.putExtra("buy_code", 2);
                setResult(ReadPdfActivity.RESULT_CODE_READ_PDF, intent);
                finish();
                break;
            case R.id.txt_post_ctph_buy:
                intent.putExtra("buy_code", 3);
                setResult(ReadPdfActivity.RESULT_CODE_READ_PDF, intent);
                finish();
                break;
            case R.id.linear_cancel:
                intent.putExtra("buy_code", -1);
                setResult(ReadPdfActivity.RESULT_CODE_READ_PDF, intent);
                finish();
        }
    }
}
