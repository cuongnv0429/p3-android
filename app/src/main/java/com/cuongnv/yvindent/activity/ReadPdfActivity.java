package com.cuongnv.yvindent.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.TextView;

import com.artifex.mupdflib.MuPDFCore;
import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.adapter.PDFPagerAdapter;
import com.cuongnv.yvindent.adapter.PDFThumbnailPagerAdapter;
import com.cuongnv.yvindent.common.ProgressDlg;
import com.cuongnv.yvindent.databinding.ActivityReadPdfBinding;
import com.cuongnv.yvindent.model.UserInfor;
import com.cuongnv.yvindent.network.ApiServices;
import com.cuongnv.yvindent.task.SafeAsyncTask;
import com.cuongnv.yvindent.utils.BaseActivity;
import com.cuongnv.yvindent.utils.BaseApplication;
import com.cuongnv.yvindent.view.RecyclerItemClickListener;
import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;


public class ReadPdfActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    public static final int REQUEST_CODE_READ_PDF = 100;
    public static final int RESULT_CODE_READ_PDF = 101;
    String postID = "";
    ActivityReadPdfBinding mActivityReadPdfBinding;
    BaseActivity baseActivity = this;
    private Subscription subscription;
    private ApiServices apiServices;
    private BaseApplication mBaseApplication;
    private String TAG = "ReadPdfActivity";
    private String fileNamePdf = "";
    private int isDemo = 1;
    private int point = 0;
    private boolean isShowPopup = false;
    private UserInfor userInfor;
    private PDFThumbnailPagerAdapter mPdfThumbnailPagerAdapter;
    private MuPDFCore mCore;
    private int currentlyViewing;
    private LinearLayoutManager mLayoutManager;
    private BitmapWorkerTask mBitmapWorkerTask;
    private HandlerPagePDF mHandlerPagePDF;
    private HandlerViewPDF mHandlerViewPDF;
    //Adapter PDF
    private PDFPagerAdapter mPdfPagerAdapter;
    private ProgressDlg mProgress;
    private PdfRenderer.Page mCurrentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mActivityReadPdfBinding == null) {
            mActivityReadPdfBinding = DataBindingUtil.setContentView(this, R.layout.activity_read_pdf);
            initView();
            getLinkReadPdf(postID, baseActivity.getUserId(baseActivity), baseActivity.getToken(baseActivity));
        }
    }

    @Override
    protected void onDestroy() {
        if (mCore != null)
            mCore.onDestroy();
        mCore = null;
        super.onDestroy();
        if (mBitmapWorkerTask != null) {
            mBitmapWorkerTask.cancel(true);
        }
        if (mHandlerPagePDF != null) {
            mHandlerPagePDF.cancel(true);
        }
        if (mHandlerViewPDF != null) {
            mHandlerViewPDF.cancel(true);
        }
    }

    private void initView() {
        isShowPopup = false;
        showProgress();
        mBaseApplication = BaseApplication.getInstance(this);
        apiServices = mBaseApplication.getApiServices();
        postID = getIntent().getStringExtra("postID");
        userInfor = baseActivity.getUserInfo();

        mActivityReadPdfBinding.linearCall.setOnClickListener(this);
        mActivityReadPdfBinding.txtExits.setOnClickListener(this);
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mActivityReadPdfBinding.recyclerView.setLayoutManager(mLayoutManager);
        mActivityReadPdfBinding.recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        if (mActivityReadPdfBinding.readerView.getCurrentItem() == position) return;
                        mActivityReadPdfBinding.readerView.setCurrentItem(position);

                    }
                })
        );
    }

    private void getLinkReadPdf(String postID, String userID, String token) {
//        Log.e(TAG, postID + "::" + userID + "::" + token);
        subscription = apiServices.readPdf(postID, userID, token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(mBaseApplication.subscribeScheduler())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject o) {
                        if (o.get("status").getAsInt() == 200) {
                            JsonObject dataObject = o.getAsJsonObject("data");
                            String url = dataObject.get("url").getAsString();
                            if (dataObject.has("is_demo")) {
                                isDemo = dataObject.get("is_demo").getAsInt();
                            }
                            if (dataObject.has("point")) {
                                point = dataObject.get("point").getAsInt();
                            }
                            if (isDemo == 1) {
                                mActivityReadPdfBinding.linearCall.setVisibility(View.VISIBLE);
                            }
                            if (!url.equalsIgnoreCase("")) {
                                fileNamePdf = baseActivity.md5(url);
                                File file = new File(getCacheDir() + "/pdf/" + fileNamePdf);
                                if (!file.exists()) {
                                    new DownloadTask().execute(url);
                                    return;
                                }
                                readPdf(file);
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        Log.e(TAG, throwable.toString());
                    }
                });
    }

    private void showProgress() {
        if (mProgress == null) {
            mProgress = new ProgressDlg(baseActivity);
            mProgress.show("Loading...");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linear_call:
                showPopupPostBuy(ReadPdfActivity.this);
                break;
            case R.id.txt_exits:
                finish();
                break;
        }
    }


    private class DownloadTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... voids) {
//            String extStorageDirectory = Environment.getExternalStorageDirectory()
//                    .toString();
            File folder = new File(getCacheDir(), "pdf");
            folder.mkdir();
            File file = new File(folder, fileNamePdf);
            try {
                file.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            try {

                FileOutputStream f = new FileOutputStream(file);
                URL u = new URL(voids[0]);
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();

                InputStream in = c.getInputStream();

                byte[] buffer = new byte[2048];
                int len1 = 0;
                while ((len1 = in.read(buffer)) > 0) {
                    f.write(buffer, 0, len1);
                }
                f.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            File file = new File(getCacheDir() + "/pdf/" + fileNamePdf);
            readPdf(file);
        }
    }

    //Read file pdf
    private void readPdf(File file) {
        try {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                HandlerPdfApi21 handlerPdfApi21 = new HandlerPdfApi21(file);
                handlerPdfApi21.execute();
                return;
            }
            mCore = new MuPDFCore(this, file.getAbsolutePath());

            mHandlerPagePDF = new HandlerPagePDF();
            mHandlerPagePDF.safeExecute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ProgressDialog showPopupPostBuy(Context context) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, null, null, true);
        progressDialog.setContentView(R.layout.popup_post_buy);
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        TextView txtOk = (TextView) progressDialog.findViewById(R.id.txt_ok);
        txtOk.setText(getResources().getString(R.string.text_call));
        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCallPhonePermissionGranted()) {
                    progressDialog.dismiss();
                    if (isCallPhonePermissionGranted()) {
                        call();
                    }
                }
            }
        });

        TextView txtCancel = (TextView) progressDialog.findViewById(R.id.txt_cancel);
        txtCancel.setText(getResources().getString(R.string.ok));
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.dismiss();
            }
        });
        progressDialog.show();
        return progressDialog;
    }

    public ProgressDialog showPopupPointBuy(Context context) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, null, null, true);
        progressDialog.setContentView(R.layout.alert_point_not_enough);
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        TextView txtOk = (TextView) progressDialog.findViewById(R.id.txt_ok);
        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReadPdfActivity.this, SettingActivity.class);
                startActivity(intent);
                finish();
                progressDialog.dismiss();
            }
        });

        TextView txtCancel = (TextView) progressDialog.findViewById(R.id.txt_cancel);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.dismiss();
            }
        });
        progressDialog.show();
        return progressDialog;
    }

    private void call() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+84822251119"));
        startActivity(intent);
    }

    public boolean isCallPhonePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(baseActivity, Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(baseActivity, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_READ_PDF && resultCode == RESULT_CODE_READ_PDF) {
            int choosePosition = data.getIntExtra("buy_code", -1);
            int point = 0;
            switch (choosePosition) {
                case 0:
                    point = 10;
                    break;
                case 1:
                    point = 120;
                    break;
                case 2:
                case 3:
                    point = 1200;
                    break;
                default:
                    point = 0;
                    return;
            }
            if (userInfor != null) {
                if (point <= userInfor.getTotal_point()) {

                } else {
                    showPopupPointBuy(ReadPdfActivity.this);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            call();
        }
    }

    private void setCurrentlyViewedPreview() {
        int i = mActivityReadPdfBinding.readerView.getCurrentItem();
        if (mCore != null) {
            if (mCore.getDisplayPages() == 2) {
                i = (i * 2) - 1;
            }
        }
        mActivityReadPdfBinding.recyclerView.smoothScrollToPosition(i);
        mPdfThumbnailPagerAdapter.setCurrentlyViewing(i);
        currentlyViewing = i;
    }


    private class BitmapWorkerTask extends AsyncTask<Void, Void, ArrayList<String>> {

        @Override
        protected ArrayList<String> doInBackground(Void... voids) {
            ArrayList<String> list = new ArrayList<>();
            Point previewSize = new Point();
            previewSize.x = getResources()
                    .getDimensionPixelSize(R.dimen.page_preview_size_width);
            previewSize.y = getResources()
                    .getDimensionPixelSize(R.dimen.page_preview_size_height);
            File cacheDirectory = new File(getCacheDir() + "/previewcache/" + fileNamePdf.replace(".pdf", ""));
            if (!cacheDirectory.exists()) {
                cacheDirectory.mkdirs();
                for (int i = 0; i < mCore.countPages(); i++) {
                    String cachedBitmapFilePath = cacheDirectory + "/" + String.valueOf(i) + ".jpg";
                    File cachedBitmapFile = new File(cachedBitmapFilePath);
                    Bitmap lq = Bitmap.createBitmap(previewSize.x, previewSize.y,
                            Bitmap.Config.ARGB_8888);
                    mCore.drawSinglePage(i, lq, previewSize.x, previewSize.y);
                    try {
                        lq.compress(Bitmap.CompressFormat.JPEG, 50, new FileOutputStream(
                                cachedBitmapFile));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        cachedBitmapFile.delete();
                    }
                    list.add(cachedBitmapFilePath);
                }
            } else {
                for (int i = 0; i < mCore.countPages(); i++) {
                    String cachedBitmapFilePath = cacheDirectory + "/" + String.valueOf(i) + ".jpg";
                    list.add(cachedBitmapFilePath);
                }
            }

            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<String> strings) {
            mPdfThumbnailPagerAdapter = new PDFThumbnailPagerAdapter(ReadPdfActivity.this, (ArrayList<String>) strings.clone());
            mActivityReadPdfBinding.recyclerView.setAdapter(mPdfThumbnailPagerAdapter);
            if (mHandlerPagePDF.getStatus() == Status.FINISHED) mProgress.hide();
        }

    }

    //Render file pdf
    private class HandlerViewPDF extends AsyncTask<Void, Void, ArrayList<String>> {
        private int width = mActivityReadPdfBinding.readerView.getWidth();
        private int height = mActivityReadPdfBinding.readerView.getHeight();
        private SparseArray<PointF> mSize;

        public HandlerViewPDF(SparseArray<PointF> size) {
            this.mSize = size;
        }

        @Override
        protected ArrayList<String> doInBackground(Void... voids) {
            ArrayList<String> list = new ArrayList<>();
            Point previewSize = new Point();
            previewSize.x = width;
            previewSize.y = height;
            int lenSize = mSize.size();
            File cacheDirectory = new File(getCacheDir() + "/previewcache/viewpdf/" + fileNamePdf.replace(".pdf", ""));
            if (!cacheDirectory.exists()) {
                cacheDirectory.mkdirs();
                for (int i = 0; i < lenSize; i++) {
                    PointF pointF = mSize.get(i);
                    float sourceScale = Math.min(previewSize.x / pointF.x, previewSize.y / pointF.y);
                    Point newSize = new Point((int) (pointF.x * sourceScale), (int) (pointF.y * sourceScale));
                    String cachedBitmapFilePath = cacheDirectory + "/" + String.valueOf(i) + ".jpg";
                    File cachedBitmapFile = new File(cachedBitmapFilePath);
                    Bitmap lq = Bitmap.createBitmap(newSize.x, newSize.y,
                            Bitmap.Config.ARGB_8888);
                    mCore.drawSinglePage(i, lq, newSize.x, newSize.y);
                    try {
                        lq.compress(Bitmap.CompressFormat.JPEG, 50, new FileOutputStream(
                                cachedBitmapFile));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        cachedBitmapFile.delete();
                    }
                    list.add(cachedBitmapFilePath);

                }
            } else {
                for (int i = 0; i < lenSize; i++) {
                    String cachedBitmapFilePath = cacheDirectory + "/" + String.valueOf(i) + ".jpg";
                    list.add(cachedBitmapFilePath);
                }
            }

            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<String> list) {
            if (mBitmapWorkerTask.getStatus() == Status.FINISHED) mProgress.hide();
            mPdfPagerAdapter = new PDFPagerAdapter(list);
            mActivityReadPdfBinding.readerView.setAdapter(mPdfPagerAdapter);
            mActivityReadPdfBinding.readerView.addOnPageChangeListener(ReadPdfActivity.this);
        }
    }

    //Get all point on pdf
    private class HandlerPagePDF extends SafeAsyncTask<Void, Void, SparseArray<PointF>> {
        @Override
        protected SparseArray<PointF> doInBackground(Void... voids) {
            int len = mCore.countPages();
            SparseArray<PointF> mPageSizes = new SparseArray<>();
            for (int i = 0; i < len; i++) {
                PointF pointF = mCore.getPageSize(i);
                mPageSizes.put(i, pointF);
            }

            return mPageSizes;
        }

        @Override
        protected void onPostExecute(SparseArray<PointF> pointFSparseArray) {
            if (isCancelled()) {
                return;
            }
            mHandlerViewPDF = new HandlerViewPDF(pointFSparseArray);
            mHandlerViewPDF.execute();
            mBitmapWorkerTask = new BitmapWorkerTask();
            mBitmapWorkerTask.execute();
            if (isDemo == 1) {
                showPopupPostBuy(baseActivity);
            }
        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setCurrentlyViewedPreview();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * api android  >= 21
     */
    private class HandlerPdfApi21 extends AsyncTask<Void, Void, Integer> {
        private File mFile;
        private ArrayList<String> pdfViewList = new ArrayList<>();
        private File folderPDFView;

        public HandlerPdfApi21(File file) {
            this.mFile = file;
            folderPDFView = new File(getCacheDir() + "/viewpdf/" + fileNamePdf.replace(".pdf", ""));
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int len = 0;
            try {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    ParcelFileDescriptor fileDescriptor = ParcelFileDescriptor.open(mFile, ParcelFileDescriptor.MODE_READ_ONLY);
                    // This is the PdfRenderer we use to render the PDF.
                    PdfRenderer pdfRenderer = new PdfRenderer(fileDescriptor);
                    len = pdfRenderer.getPageCount();

                    if (!folderPDFView.exists()) {
                        folderPDFView.mkdirs();
                        for (int i = 0; i < len; i++) {
                            if (null != mCurrentPage) {
                                mCurrentPage.close();
                            }
                            mCurrentPage = pdfRenderer.openPage(i);
                            Bitmap bitmap = Bitmap.createBitmap(mCurrentPage.getWidth(), mCurrentPage.getHeight(),
                                    Bitmap.Config.ARGB_8888);
                            // Here, we render the page onto the Bitmap.
                            // To render a portion of the page, use the second and third parameter. Pass nulls to get
                            // the default result.
                            // Pass either RENDER_MODE_FOR_DISPLAY or RENDER_MODE_FOR_PRINT for the last parameter.
                            mCurrentPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                            String pathPDFView = folderPDFView + "/" + String.valueOf(i) + ".png";
                            persistImage(bitmap, pathPDFView);
                            pdfViewList.add(pathPDFView);
                        }
                        if (null != mCurrentPage) {
                            mCurrentPage.close();
                        }
                        pdfRenderer.close();
                        fileDescriptor.close();


                    }
                }
            } catch (Exception e) {

            }

            return len;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            if (pdfViewList.size() == 0) {
                for (int i = 0; i < integer; i++) {
                    String pathPDFView = folderPDFView + "/" + String.valueOf(i) + ".png";
                    pdfViewList.add(pathPDFView);
                }
            }
            mPdfThumbnailPagerAdapter = new PDFThumbnailPagerAdapter(ReadPdfActivity.this, pdfViewList);
            mActivityReadPdfBinding.recyclerView.setAdapter(mPdfThumbnailPagerAdapter);

            mPdfPagerAdapter = new PDFPagerAdapter(pdfViewList);
            mActivityReadPdfBinding.readerView.setAdapter(mPdfPagerAdapter);
            mActivityReadPdfBinding.readerView.addOnPageChangeListener(ReadPdfActivity.this);
            mProgress.hide();
        }
    }

    private void persistImage(Bitmap bitmap, String name) {
        File imageFile = new File(name);
        try {
            imageFile.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
    }
}
