package com.cuongnv.yvindent.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.activity.ViewAdsActivity;
import com.cuongnv.yvindent.model.Ads;
import com.cuongnv.yvindent.utils.BaseActivity;
import com.cuongnv.yvindent.utils.BaseApplication;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

/**
 * Created by ITV01 on 1/19/17.
 */

public class CustomPagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    List<Ads> adsList;
    ImageLoader imageLoader;
    DisplayImageOptions defaultOptions;

    public CustomPagerAdapter(Context context, List<Ads> adses) {
        mContext = context;
        adsList = adses;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(BaseApplication.config);
        defaultOptions = BaseApplication.defaultOptions;
    }

    @Override
    public int getCount() {
        return adsList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);
        final ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        imageView.getLayoutParams().width = ((BaseActivity) mContext).getScreenWidth();
        ImageSize targetSize = new ImageSize(200, 200); // result Bitmap will be fit to this size
        imageLoader.loadImage(adsList.get(position).getImg(), targetSize, defaultOptions, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // Do whatever you want with Bitmap
                imageView.setImageBitmap(loadedImage);
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ViewAdsActivity.class);
                intent.putExtra("url", adsList.get(position).getUrlRedirect());
                mContext.startActivity(intent);
            }
        });
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
