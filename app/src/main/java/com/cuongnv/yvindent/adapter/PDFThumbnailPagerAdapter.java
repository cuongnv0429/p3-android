package com.cuongnv.yvindent.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cuongnv.yvindent.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PDFThumbnailPagerAdapter extends RecyclerView.Adapter<PDFThumbnailPagerAdapter.PDFPreviewViewHolder> {

    private Context mContext;

    private int currentlyViewing;
    private ArrayList<String> mListPathFile;

    public PDFThumbnailPagerAdapter(Context context, ArrayList<String> list) {
        mContext = context;
        this.mListPathFile = (ArrayList<String>) list.clone();
    }

    @Override
    public PDFPreviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.preview_pager_item_layout, parent, false);
        return new PDFPreviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PDFPreviewViewHolder holder, int position) {
        if (getCurrentlyViewing() == position) {
            holder.previewPageLinearLayout
                    .setBackgroundColor(mContext
                            .getResources()
                            .getColor(
                                    R.color.thumbnail_selected_background));
            ScaleAnimation fade_in = new ScaleAnimation(1f, 1.3f, 1f, 1.3f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            fade_in.setDuration(100);     // animation duration in milliseconds
            fade_in.setFillAfter(true);    // If fillAfter is true, the transformation that this animation performed will persist when it is finished.
            holder.itemView.startAnimation(fade_in);
        } else {
            holder.previewPageLinearLayout
                    .setBackgroundColor(Color.TRANSPARENT);
        }
        String path = mListPathFile.get(position);
        Picasso.with(mContext)
                .load("file:///" + path)
                .fit()
//                .resize(mContext.getResources().getDimensionPixelSize(R.dimen.page_preview_size_width),
//                        mContext.getResources().getDimensionPixelSize(R.dimen.page_preview_size_height))
                .into(holder.previewPageImageView);
    }

    @Override
    public long getItemId(int pPosition) {
        if (pPosition > 0)
            return (pPosition + 1) / 2;
        else
            return 0;
    }

    @Override
    public int getItemCount() {
        return mListPathFile.size();
    }

    public int getCurrentlyViewing() {
        return currentlyViewing;
    }

    public void setCurrentlyViewing(int currentlyViewing) {
        this.currentlyViewing = currentlyViewing;
        notifyDataSetChanged();
    }

    public class PDFPreviewViewHolder extends RecyclerView.ViewHolder {

        ImageView previewPageImageView = null;
        LinearLayout previewPageLinearLayout = null;

        public PDFPreviewViewHolder(View view) {
            super(view);
            this.previewPageImageView = (ImageView) view
                    .findViewById(R.id.PreviewPageImageView);
            this.previewPageLinearLayout = (LinearLayout) view
                    .findViewById(R.id.PreviewPageLinearLayout);
        }
    }
}
