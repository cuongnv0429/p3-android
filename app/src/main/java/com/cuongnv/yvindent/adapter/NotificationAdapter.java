package com.cuongnv.yvindent.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.model.Notification;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by ITV01 on 4/6/17.
 */

public class NotificationAdapter extends BaseAdapter {
    Context mContext;
    List<Notification> mListNotifications;

    public NotificationAdapter(Context mContext, List<Notification> mList) {
        this.mContext = mContext;
        this.mListNotifications = mList;
    }

    @Override
    public int getCount() {
        return mListNotifications.size();
    }

    @Override
    public Object getItem(int position) {
        return mListNotifications.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_notifications, parent, false);

        TextView txtContent = (TextView) rowView.findViewById(R.id.txt_conttent);
        TextView txtDate = (TextView) rowView.findViewById(R.id.txt_time);

        Notification mNotification = mListNotifications.get(position);
        if (mNotification != null) {
            txtContent.setText(mNotification.getName());
            Timestamp mTimeStamp = new Timestamp(Long.parseLong(mNotification.getDate()));
            java.util.Date mDate =  new Date(mTimeStamp.getTime() * 1000L);
            // S is the millisecond
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
            txtDate.setText(simpleDateFormat.format(mDate));

        }
        return rowView;
    }
}

