package com.cuongnv.yvindent.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.model.Post;
import com.cuongnv.yvindent.utils.BaseActivity;
import com.cuongnv.yvindent.utils.BaseApplication;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

/**
 * Created by ITV01 on 1/16/17.
 */

public class ListPostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int VIEW_TYPE_LOADING = 2;
    private static final int VIEW_TYPE_ITEM = 1;
    private List<Post> postsList;
    private Context mContext;
    ImageLoader imageLoader;
    DisplayImageOptions defaultOptions;

    public ListPostAdapter(Context mContext, List<Post> postsList) {
        this.mContext = mContext;
        this.postsList = postsList;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(BaseApplication.config);
        defaultOptions = BaseApplication.defaultOptions;
    }

    public void setList(List<Post> postsList) {
        this.postsList = postsList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View v = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_header, parent, false);
            return new HeaderViewHolder (v);
        } else if(viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_loading, parent, false);
            return new FooterViewHolder (v);
        } else if(viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_recycler_list_product, parent, false);
            return new ListPostAdapterViewHolder(v);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderViewHolder) {
            final HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            Post post = postsList.get(position);
            headerViewHolder.txtHeaderDate.setText(post.getDate());
            headerViewHolder.txtHeaderTitle.setText(post.getTitle());
            headerViewHolder.txtHeaderDescription.setText(post.getDescription());
            headerViewHolder.imgHeaderThumb.getLayoutParams().width = (int) (((BaseActivity) mContext).getScreenWidth() * 0.5);
            headerViewHolder.imgHeaderThumb.getLayoutParams().height = (int) (((BaseActivity) mContext).getScreenWidth() * 0.6);
            ImageSize targetSize = new ImageSize(300, 250); // result Bitmap will be fit to this size
            imageLoader.loadImage(post.getThumbImg(), targetSize, defaultOptions, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    // Do whatever you want with Bitmap
                    headerViewHolder.imgHeaderThumb.setImageBitmap(loadedImage);
                }
            });
        }
        if(holder instanceof FooterViewHolder) {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;
            footerHolder.progressBar.setIndeterminate(true);
        } else if(holder instanceof ListPostAdapterViewHolder) {
            final ListPostAdapterViewHolder listPostAdapterViewHolder = (ListPostAdapterViewHolder) holder;
            Post post = postsList.get(position);
            listPostAdapterViewHolder.txtDate.setText(post.getDate());
            listPostAdapterViewHolder.imgThumb.getLayoutParams().width = ((BaseActivity) mContext).getScreenWidth() / 2 - 20;
            listPostAdapterViewHolder.imgThumb.getLayoutParams().height = (int) (((BaseActivity) mContext).getScreenWidth() * 0.6);
            ImageSize targetSize = new ImageSize(300, 250); // result Bitmap will be fit to this size
            imageLoader.loadImage(post.getThumbImg(), targetSize, defaultOptions, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    // Do whatever you want with Bitmap
                    listPostAdapterViewHolder.imgThumb.setImageBitmap(loadedImage);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return postsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isHeader(position)) {
            return 0;
        }
        return postsList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public boolean isHeader(int position) {
        return position == 0;
    }

    public  class FooterViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public FooterViewHolder (View itemView) {
            super (itemView);
            this.progressBar = (ProgressBar) itemView.findViewById (R.id.progressBar);
        }
    }

    public class ListPostAdapterViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgThumb;
        public TextView txtDate;
        public ListPostAdapterViewHolder(View view) {
            super(view);
            imgThumb = (ImageView) view.findViewById(R.id.img_thumb);
            txtDate = (TextView) view.findViewById(R.id.txt_date);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public TextView txtHeaderDate, txtHeaderTitle, txtHeaderDescription, txtHeaderRead;
        public ImageView imgHeaderThumb;
        public HeaderViewHolder(View view) {
            super(view);
            txtHeaderDate = (TextView) view.findViewById(R.id.txt_header_date);
            txtHeaderTitle = (TextView) view.findViewById(R.id.txt_header_title);
            txtHeaderDescription = (TextView) view.findViewById(R.id.txt_header_description);
            txtHeaderRead = (TextView) view.findViewById(R.id.txt_header_read);
            imgHeaderThumb = (ImageView) view.findViewById(R.id.img_header_thumb);
        }
    }
}
