package com.cuongnv.yvindent.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.model.Category;

import java.util.List;

/**
 * Created by ITV01 on 1/15/17.
 */

public class CustomDrawerAdapter extends ArrayAdapter<Category> {

    Context context;
    List<Category> drawerItemList;
    int layoutResID;

    public CustomDrawerAdapter(Context context, int layoutResourceID,
                               List<Category> listItems) {
        super(context, layoutResourceID, listItems);
        this.context = context;
        this.drawerItemList = listItems;
        this.layoutResID = layoutResourceID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        DrawerItemHolder drawerHolder;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            drawerHolder = new DrawerItemHolder();

            view = inflater.inflate(layoutResID, parent, false);
            drawerHolder.txtItemName = (TextView) view
                    .findViewById(R.id.drawer_itemName);
            drawerHolder.imgDrawer = (ImageView) view.findViewById(R.id.img_drawer);
            view.setTag(drawerHolder);
        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();

        }

        Category category = (Category) this.drawerItemList.get(position);
        if (category.getIdImages() == 0) {
            drawerHolder.imgDrawer.setVisibility(View.GONE);
            drawerHolder.txtItemName.setVisibility(View.VISIBLE);
            drawerHolder.txtItemName.setText(category.getNameCategory());
        } else {
            drawerHolder.imgDrawer.setVisibility(View.VISIBLE);
            drawerHolder.txtItemName.setVisibility(View.GONE);
            drawerHolder.imgDrawer.setBackgroundResource(category.getIdImages());
        }
        return view;
    }

    private class DrawerItemHolder {
        TextView txtItemName;
        ImageView imgDrawer;
    }
}
