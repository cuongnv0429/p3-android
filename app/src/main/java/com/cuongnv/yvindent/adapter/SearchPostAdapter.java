package com.cuongnv.yvindent.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.model.Post;
import com.cuongnv.yvindent.utils.BaseActivity;
import com.cuongnv.yvindent.utils.BaseApplication;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

/**
 * Created by ITV01 on 1/20/17.
 */

public class SearchPostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int VIEW_TYPE_LOADING = 2;
    private static final int VIEW_TYPE_ITEM = 1;
    private List<Post> postsList;
    private Context mContext;
    ImageLoader imageLoader;
    DisplayImageOptions defaultOptions;

    public SearchPostAdapter(Context mContext, List<Post> postsList) {
        this.mContext = mContext;
        this.postsList = postsList;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(BaseApplication.config);
        defaultOptions = BaseApplication.defaultOptions;
    }

    public void setList(List<Post> postsList) {
        this.postsList = postsList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_loading, parent, false);
            return new SearchPostAdapter.FooterViewHolder(v);
        } else if(viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_recycler_list_product, parent, false);
            return new SearchPostAdapter.SearchPostAdapterViewHolder(v);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof SearchPostAdapter.FooterViewHolder) {
            SearchPostAdapter.FooterViewHolder footerHolder = (SearchPostAdapter.FooterViewHolder) holder;
            footerHolder.progressBar.setIndeterminate(true);
        } else if(holder instanceof SearchPostAdapter.SearchPostAdapterViewHolder) {
            final SearchPostAdapter.SearchPostAdapterViewHolder searchPostAdapterViewHolder = (SearchPostAdapter.SearchPostAdapterViewHolder) holder;
            Post post = postsList.get(position);
            searchPostAdapterViewHolder.txtDate.setText(post.getDate());
            searchPostAdapterViewHolder.imgThumb.getLayoutParams().width = (int) ((BaseActivity) mContext).getScreenWidth() / 2 - 20;
            searchPostAdapterViewHolder.imgThumb.getLayoutParams().height = (int) (((BaseActivity) mContext).getScreenWidth() * 0.6);
            //download ảnh và show ImageView tại đây
            ImageSize targetSize = new ImageSize(300, 250); // result Bitmap will be fit to this size
            imageLoader.loadImage(post.getThumbImg(), targetSize, defaultOptions, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    // Do whatever you want with Bitmap
                    searchPostAdapterViewHolder.imgThumb.setImageBitmap(loadedImage);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return postsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return postsList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public  class FooterViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public FooterViewHolder (View itemView) {
            super (itemView);
            this.progressBar = (ProgressBar) itemView.findViewById (R.id.progressBar);
        }
    }

    public class SearchPostAdapterViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgThumb;
        public TextView txtDate;
        public SearchPostAdapterViewHolder(View view) {
            super(view);
            imgThumb = (ImageView) view.findViewById(R.id.img_thumb);
            txtDate = (TextView) view.findViewById(R.id.txt_date);
        }
    }
}