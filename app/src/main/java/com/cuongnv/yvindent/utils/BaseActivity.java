package com.cuongnv.yvindent.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.model.UserInfor;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.realm.Realm;

import static com.cuongnv.yvindent.utils.BaseApplication.config;
import static com.cuongnv.yvindent.utils.BaseApplication.defaultOptions;

/**
 * Created by ITV01 on 1/11/17.
 */

public class BaseActivity extends AppCompatActivity{
    private Realm mRealm;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        mRealm = Realm.getDefaultInstance();
    }

    public Realm getmRealm() {
        return mRealm;
    }

    public  ImageLoaderConfiguration getConfig() {
        return config;
    }

    public  DisplayImageOptions getDefaultOptions() {
        return defaultOptions;
    }

    public  void vibarte(Context context) {
        if(context != null) {
            Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            v.vibrate(Constant.VIBRATE);
        }
    }

    public void saveToken(BaseActivity baseActivity, String token) {
        SharedPreferences mySharedPreferences = getSharedPreferences("yvindent", MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor = mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.TOKEN, token);
        sharedpreferenceeditor.commit();
    }

    public String getToken(BaseActivity baseActivity) {
        SharedPreferences mySharedPreferences = getSharedPreferences("yvindent", MODE_PRIVATE);
        return mySharedPreferences.getString(Constant.TOKEN, "");
    }

    public void saveUserId(BaseActivity baseActivity, String token) {
        SharedPreferences mySharedPreferences = getSharedPreferences("yvindent", MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor = mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.USER_ID, token);
        sharedpreferenceeditor.commit();
    }

    public String getUserId(BaseActivity baseActivity) {
        SharedPreferences mySharedPreferences = getSharedPreferences("yvindent", MODE_PRIVATE);
        return mySharedPreferences.getString(Constant.USER_ID, "");
    }

    public void saveIsLogin(BaseActivity baseActivity, Boolean isLogin) {
        SharedPreferences mySharedPreferences = getSharedPreferences("yvindent", MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor=mySharedPreferences.edit();
        sharedpreferenceeditor.putBoolean(Constant.IS_LOGIN, isLogin);
        sharedpreferenceeditor.commit();
    }

    public Boolean getIsLogin(BaseActivity baseActivity) {
        SharedPreferences mySharedPreferences = getSharedPreferences("yvindent", MODE_PRIVATE);
        return mySharedPreferences.getBoolean(Constant.IS_LOGIN, false);
    }

    public UserInfor getUserInfo() {
        UserInfor userInfor;
        if (mRealm == null) {
            mRealm = Realm.getDefaultInstance();
        }
        userInfor = mRealm.where(UserInfor.class).findFirst();
        return userInfor;
    }

    public void saveUserInfor(BaseActivity baseActivity, UserInfor user) {
        if (mRealm == null) {
            mRealm = Realm.getDefaultInstance();
        }
        mRealm.beginTransaction();

        if (getUserInfo() != null) {
            mRealm.where(UserInfor.class).findAll().clear();
        }
        UserInfor userInfor = new UserInfor();
        userInfor = mRealm.createObject(UserInfor.class);
        if (user != null) {
            userInfor.setUsername(user.getUsername());
            userInfor.setDob(user.getDob());
            userInfor.setFullname(user.getFullname());
            userInfor.setGender(user.getGender());
            userInfor.setAddress(user.getAddress());
            userInfor.setPhone(user.getPhone());
            userInfor.setEmail(user.getEmail());
            userInfor.setId(user.getId());
            userInfor.setLevel(user.getLevel());
            userInfor.setPoint(user.getPoint());
            userInfor.setStatus(user.getStatus());
            userInfor.setToken(user.getToken());
            userInfor.setTotal_point(user.getTotal_point());
            userInfor.setTotalPoint(user.getTotalPoint());
        }
        mRealm.commitTransaction();
    }

    public ProgressDialog showLoading(Context context) {
        ProgressDialog progressDialog = ProgressDialog.show(context, null, null, true);
        progressDialog.setContentView(R.layout.progress_loading);
        if(progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        progressDialog.show();
        return progressDialog;
    }

    public  void hideLoading(ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public void keyboardShow(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public  int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public  int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public void alert(Context mContext, String alert) {
        // custom dialog
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert);
        // set the custom dialog components - text, image and button
        TextView textView = (TextView) dialog.findViewById(R.id.txt_dialog_alert);
        textView.setText(alert);
        dialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 1000);
    }

    public int getScreenWidth(Context ctx) {
        int w = 0;
        if (ctx instanceof Activity) {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            ((Activity) ctx).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            w = displaymetrics.widthPixels;
        }
        return w;
    }

    public void savePushToken(BaseActivity baseActivity, String pushToken) {
        SharedPreferences mySharedPreferences = getSharedPreferences("yvindent", MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor = mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.PUSH_TOKEN, pushToken);
        sharedpreferenceeditor.commit();
    }

    public String getPushToken(BaseActivity baseActivity) {
        SharedPreferences mySharedPreferences = getSharedPreferences("yvindent", MODE_PRIVATE);
        return mySharedPreferences.getString(Constant.PUSH_TOKEN, "");
    }

    public String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i=0; i<messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
