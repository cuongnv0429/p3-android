package com.cuongnv.yvindent.utils;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.cuongnv.yvindent.network.ApiFactory;
import com.cuongnv.yvindent.network.ApiServices;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 1/12/17.
 */

public class BaseApplication extends Application{
    private ApiServices apiServices;
    private Scheduler scheduler;
    public static ImageLoaderConfiguration config;
    public static DisplayImageOptions defaultOptions;

    @Override
    public void onCreate() {
        super.onCreate();
        //creat realm
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

        defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();
        File cacheDir = StorageUtils.getCacheDirectory(this);
        config = new ImageLoaderConfiguration.Builder(this)
                .threadPriority(Thread.NORM_PRIORITY-2)
                .threadPoolSize(3)
                .diskCache(new UnlimitedDiskCache(cacheDir))
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .diskCacheExtraOptions(720, 1280, null)
                .memoryCacheSizePercentage(20)
                .build();
        Fabric.with(this, new Crashlytics());

    }

    public static BaseApplication getInstance(Context context) {
        return (BaseApplication) context.getApplicationContext();
    }


    public Scheduler subscribeScheduler() {
        if (scheduler == null) {
            scheduler = Schedulers.io();
        }
        return scheduler;
    }

    public ApiServices getApiServices() {
        if (apiServices == null) {
            apiServices = ApiFactory.createServices();
        }
        return apiServices;
    }
}
