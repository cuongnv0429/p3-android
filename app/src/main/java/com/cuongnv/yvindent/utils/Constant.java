package com.cuongnv.yvindent.utils;

/**
 * Created by ITV01 on 1/14/17.
 */

public class Constant {
    public static final int VIBRATE = 500;
    public static final int SUCCESS = 200;

    //
    public static final String TOKEN = "token";
    public static final String USER_ID = "id";
    public static final String IS_LOGIN = "is_login";
    public static final String PUSH_TOKEN = "push_token";

    public static final String BASE64 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0s/I+URhoOPCht6yp+Pyi+aO9dPZO39FATvDopDMAXTPBbJc8VV1JKhbsmW0W+ARDup1FN2bpkHSQCEEU95L0Cod+2gXnRNQ+V4xSGL/KLeSoZvuRNOz9Qlb8sU6l7BcRPvJD4gM17ndjWQnYFHh8CKWFM22OFZyJdRMLFgwDIjCg4ax8OHSf8fFKvT/Y8ODcyIsX1NI7V+jg0qOzM9M+X0HFw62F/KKOAvKlZ3WLmdKbw1uUvSx/+WX6/7zd3jFJsLrNnsmxTBNE7YqE+Z+Lh06xBEbwdcwHTyGTidxMc8ilESK0LBoFqa0EB8ecmfLR3WrMohg0qQGGo+565GvKQIDAQAB";
}
