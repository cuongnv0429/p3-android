package com.cuongnv.yvindent.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.activity.SettingActivity;
import com.cuongnv.yvindent.adapter.NotificationAdapter;
import com.cuongnv.yvindent.databinding.FragmentListNotificationsBinding;
import com.cuongnv.yvindent.model.Notification;
import com.cuongnv.yvindent.network.ApiServices;
import com.cuongnv.yvindent.utils.BaseActivity;
import com.cuongnv.yvindent.utils.BaseApplication;
import com.cuongnv.yvindent.utils.BaseFragment;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by ITV01 on 4/6/17.
 */

public class FragmentListNotifications extends BaseFragment{
    BaseActivity mActivity;
    FragmentListNotificationsBinding mNotificationsBinding;
    NotificationAdapter mNotificationAdapter;
    List<Notification> mListNotifications;
    BaseApplication mBaseApplication;
    ApiServices mApiServices;
    Subscription mSubscription;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if ( mNotificationsBinding == null) {
            mNotificationsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_list_notifications, container, false);
            initView();
        }
        return mNotificationsBinding.getRoot();
    }

    void initView() {
        mBaseApplication = BaseApplication.getInstance(mActivity);
        mApiServices = mBaseApplication.getApiServices();
        mNotificationsBinding.lsvListNotifications.setVisibility(View.GONE);
        mNotificationsBinding.txtNoNotification.setVisibility(View.GONE);
        mNotificationsBinding.progressBarListNotifications.setVisibility(View.VISIBLE);
        mListNotifications = new ArrayList<>();
        mNotificationAdapter = new NotificationAdapter(mActivity, mListNotifications);
        mNotificationsBinding.lsvListNotifications.setAdapter(mNotificationAdapter);
        getListNotifications();
        mNotificationsBinding.lsvListNotifications.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ((SettingActivity) mActivity).pushFragment(
                        FragmentNotificationDetail.newInstance(mListNotifications.get(position).getId()));
            }
        });

        mNotificationsBinding.imbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
    }

    private void getListNotifications() {
        mSubscription = mApiServices.getListNotifications()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(mBaseApplication.subscribeScheduler())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject o) {
                        if (o.get("status").getAsInt() == 200) {
                            List<Notification> mList = new ArrayList<Notification>();
                            Type listType = new TypeToken<List<Notification>>(){}.getType();
                            if (o.getAsJsonArray("data") != null) {
                                mList = (List<Notification>) new Gson().fromJson(o.get("data"), listType);
                                mListNotifications = mList;
                                mNotificationAdapter = new NotificationAdapter(mActivity, mListNotifications);
                                mNotificationsBinding.lsvListNotifications.setAdapter(mNotificationAdapter);
                                mNotificationsBinding.lsvListNotifications.setVisibility(View.VISIBLE);
                                mNotificationsBinding.progressBarListNotifications.setVisibility(View.GONE);
                                mNotificationsBinding.txtNoNotification.setVisibility(View.GONE);
                                return;
                            }

                        }
                        mNotificationsBinding.lsvListNotifications.setVisibility(View.GONE);
                        mNotificationsBinding.progressBarListNotifications.setVisibility(View.GONE);
                        mNotificationsBinding.txtNoNotification.setVisibility(View.VISIBLE);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mNotificationsBinding.txtNoNotification.setVisibility(View.VISIBLE);
                        mNotificationsBinding.lsvListNotifications.setVisibility(View.GONE);
                        mNotificationsBinding.progressBarListNotifications.setVisibility(View.GONE);
                    }
                });
    }
}
