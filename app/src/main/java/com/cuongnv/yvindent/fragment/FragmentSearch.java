package com.cuongnv.yvindent.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.activity.ReadPdfActivity;
import com.cuongnv.yvindent.activity.SearchActivity;
import com.cuongnv.yvindent.adapter.RecyclerItemClickListener;
import com.cuongnv.yvindent.adapter.SearchPostAdapter;
import com.cuongnv.yvindent.databinding.FragmentSearchBinding;
import com.cuongnv.yvindent.model.Post;
import com.cuongnv.yvindent.network.ApiServices;
import com.cuongnv.yvindent.utils.BaseActivity;
import com.cuongnv.yvindent.utils.BaseApplication;
import com.cuongnv.yvindent.utils.BaseFragment;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by ITV01 on 1/20/17.
 */

public class FragmentSearch extends BaseFragment implements View.OnClickListener{
    private BaseActivity baseActivity;
    private FragmentSearchBinding fragmentSearchBinding;
    private List<Post> searchPostList;
    private SearchPostAdapter searchAdapter;
    private CountDownTimer countDownTimer;
    private String querySearch;
    private Boolean isEmptySearch;
    private Subscription subscription;
    BaseApplication baseApplication;
    ApiServices apiServices;
    private String cat = "";
    private GridLayoutManager gridManager;
    private Post curretnPost;
    private String TAG = FragmentSearch.class.getName();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        baseActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (fragmentSearchBinding == null) {
            fragmentSearchBinding =  DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false);
            if (getArguments() != null) {
                cat = getArguments().getString("cat");
            }
            initView();
        }
        return fragmentSearchBinding.getRoot();
    }

    private void initView() {
        fragmentSearchBinding.toolbarSearch.lnlExitSearch.setOnClickListener(this);
        baseApplication = BaseApplication.getInstance(baseActivity);
        apiServices = baseApplication.getApiServices();
        searchPostList = new ArrayList<>();
        searchAdapter = new SearchPostAdapter(baseActivity, searchPostList);
        gridManager = new GridLayoutManager(baseActivity, 2);
        fragmentSearchBinding.recyclerSearch.setLayoutManager(gridManager);
        fragmentSearchBinding.recyclerSearch.setHasFixedSize(true);
        fragmentSearchBinding.recyclerSearch.setItemViewCacheSize(20);
        fragmentSearchBinding.recyclerSearch.setDrawingCacheEnabled(true);
        fragmentSearchBinding.recyclerSearch.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        fragmentSearchBinding.recyclerSearch.setAdapter(searchAdapter);
        fragmentSearchBinding.toolbarSearch.edtSearch.setOnClickListener(this);

        fragmentSearchBinding.recyclerSearch.addOnItemTouchListener(
                new RecyclerItemClickListener(baseActivity, fragmentSearchBinding.recyclerSearch,
                        new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // do whatever
                        curretnPost = searchPostList.get(position);
                        if (((SearchActivity) baseActivity).isStoragePermissionGranted()) {
                            Intent intent = new Intent(baseActivity, ReadPdfActivity.class);
                            intent.putExtra("postID", searchPostList.get(position).getId());
                            startActivity(intent);
                        }
                    }
                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        fragmentSearchBinding.toolbarSearch.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int index, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int index, int i1, int i2) {
                querySearch = charSequence.toString();
                if (charSequence.toString().trim().equalsIgnoreCase("")) {
                    isEmptySearch = true;
                } else {
                    isEmptySearch = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (isEmptySearch) {
                    searchPostList.clear();
                    searchAdapter.setList(searchPostList);
                } else {
                    startTimer(querySearch);
                }
            }
        });
    }

    void startTimer(final String strBefore) {
        cancelTimer();
        if (subscription != null) {
            subscription.unsubscribe();
        }
        countDownTimer = new CountDownTimer(500, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                if (strBefore.equalsIgnoreCase(fragmentSearchBinding.toolbarSearch.edtSearch.getText().toString().trim())) {
                    search(fragmentSearchBinding.toolbarSearch.edtSearch.getText().toString().trim(), cat);
                }
            }
        };
        countDownTimer.start();
    }
    void cancelTimer() {
        if(countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        baseActivity.hideKeyboard();
    }

    private void search(String keyword, String cat) {
        fragmentSearchBinding.progressBarSearch.setVisibility(View.VISIBLE);
        fragmentSearchBinding.recyclerSearch.setVisibility(View.GONE);
        if (searchPostList != null) {
            searchPostList.clear();
        }
        subscription = apiServices.search(keyword, cat)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(baseApplication.subscribeScheduler())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject o) {
                        if (o.get("status").getAsInt() == 200) {
                            List<Post> list = new ArrayList<Post>();
                            JsonArray dataArray = o.getAsJsonArray("data");
                            for (int i = 0; i < dataArray.size(); i++) {
                                JsonObject json = dataArray.get(i).getAsJsonObject();
                                JsonObject postObject = json.getAsJsonObject("Post");
                                Post post = parseJson(postObject);
                                list.add(post);
                            }
                            Log.e("search", list.size() + "::");
                            if (list.size() > 0) {
                                searchPostList.addAll(list);
                                searchAdapter.setList(searchPostList);
                            }
                        }
                        fragmentSearchBinding.progressBarSearch.setVisibility(View.GONE);
                        fragmentSearchBinding.recyclerSearch.setVisibility(View.VISIBLE);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        fragmentSearchBinding.progressBarSearch.setVisibility(View.GONE);
                    }
                });
    }

    private Post parseJson(JsonObject json) {
        Post post = new Post();
        if (json.has("id")) {
            post.setId(json.get("id").getAsString());
        }
        if (json.has("title")) {
            try {
                post.setTitle(json.get("title").getAsString());
            } catch (UnsupportedOperationException un) {
                un.printStackTrace();
            }
        }
        if (json.has("date")) {
            try {
                post.setDate(json.get("date").getAsString());
            } catch (UnsupportedOperationException un) {
                un.printStackTrace();
            }
        }
        if (json.has("thumbImg")) {
            try {
                post.setThumbImg(json.get("thumbImg").getAsString());
            } catch (UnsupportedOperationException un) {
                un.printStackTrace();
            }
        }
        if (json.has("urlDemo")) {
            try {
                post.setUrlDemo(json.get("urlDemo").getAsString());
            } catch (UnsupportedOperationException un) {
                un.printStackTrace();
            }
        }
        if (json.has("description")) {
            try {
                post.setDescription(json.get("description").getAsString());
            } catch (UnsupportedOperationException un) {
                un.printStackTrace();
            }
        }
        if (json.has("stt")) {
            try
            {
                post.setStt(json.get("stt").getAsString());
            } catch (UnsupportedOperationException un) {
                un.printStackTrace();
            }
        }
        if (json.has("categories")) {
            try {
                post.setCategories(json.get("categories").getAsString());
            } catch (UnsupportedOperationException un) {
                un.printStackTrace();
            }
        }
        return post;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edt_search:
                baseActivity.keyboardShow(fragmentSearchBinding.toolbarSearch.edtSearch);
                break;
            case R.id.lnl_exit_search:
                baseActivity.hideKeyboard();
                baseActivity.finish();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Intent intent = new Intent(baseActivity, ReadPdfActivity.class);
            intent.putExtra("postID", curretnPost.getId());
            startActivity(intent);
        }
    }
}
