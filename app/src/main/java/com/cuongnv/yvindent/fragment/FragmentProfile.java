package com.cuongnv.yvindent.fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.activity.MainActivity;
import com.cuongnv.yvindent.activity.SettingActivity;
import com.cuongnv.yvindent.databinding.FragmentProfileBinding;
import com.cuongnv.yvindent.model.UpdateInforRequest;
import com.cuongnv.yvindent.model.UserInfor;
import com.cuongnv.yvindent.network.ApiServices;
import com.cuongnv.yvindent.utils.BaseActivity;
import com.cuongnv.yvindent.utils.BaseApplication;
import com.cuongnv.yvindent.utils.BaseFragment;
import com.cuongnv.yvindent.utils.Constant;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by ITV01 on 1/24/17.
 */

public class FragmentProfile extends BaseFragment implements View.OnClickListener{
    private String TAG = "FragmentProfile";
    static final String ITEM_SKU = "android.test.purchased";
    private BaseActivity baseActivity;
    private FragmentProfileBinding fragmentProfileBinding;
    UserInfor mUser;
    Realm mRealm;
    Calendar mCalendar;
    DatePickerDialog.OnDateSetListener date;
    private ProgressDialog mProgress;
    BaseApplication baseApplication;
    ApiServices apiServices;
    Subscription subscription;
    private String gender = "0";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        baseActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (fragmentProfileBinding == null) {
            fragmentProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
            initView();
        }
        return fragmentProfileBinding.getRoot();
    }

    private void initView() {
        mRealm = baseActivity.getmRealm();
        if (mRealm == null) {
            mRealm = Realm.getDefaultInstance();
        }
        mUser = new UserInfor();
        mUser = baseActivity.getUserInfo();
        mCalendar = Calendar.getInstance();
        baseApplication = BaseApplication.getInstance(baseActivity);
        apiServices = baseApplication.getApiServices();

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, monthOfYear);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateBirthDay(mCalendar);
            }

        };
        fragmentProfileBinding.txtLogout.setText(Html.fromHtml(getResources().getString(R.string.text_logout_profile)));
        if (mUser != null) {
            if (!mUser.getFullname().equalsIgnoreCase("")) {
                fragmentProfileBinding.edtFullName.setText(mUser.getFullname());
            } else {
                fragmentProfileBinding.edtFullName.setText(mUser.getUsername());
            }
            fragmentProfileBinding.edtAdress.setText(mUser.getAddress());
            fragmentProfileBinding.edtEmail.setText(mUser.getEmail());
            fragmentProfileBinding.edtPhoneNumber.setText(mUser.getPhone());
            fragmentProfileBinding.txtBirthday.setText(mUser.getDob());
            gender = mUser.getGender();
            if (gender.equalsIgnoreCase("0")) {
                fragmentProfileBinding.linearMale.setBackgroundResource(R.drawable.bg_linear_male_no_choose);
                fragmentProfileBinding.imgMale.setBackgroundResource(R.drawable.icon_male_choose_no);
                fragmentProfileBinding.txtMale.setTextColor(getResources().getColor(R.color.colorTextSignup));

                fragmentProfileBinding.linearFemale.setBackgroundResource(R.drawable.bg_linear_female_choose);
                fragmentProfileBinding.imgFemale.setBackgroundResource(R.drawable.icon_female_choose);
                fragmentProfileBinding.txtFemale.setTextColor(getResources().getColor(R.color.white));

                fragmentProfileBinding.linearMale.setBackgroundResource(R.drawable.bg_linear_male_no_choose);
                fragmentProfileBinding.imgAvatar.setBackgroundResource(R.drawable.icon_avatar_female);
            } else {
                fragmentProfileBinding.imgMale.setBackgroundResource(R.drawable.icon_male_choose);
                fragmentProfileBinding.txtMale.setTextColor(getResources().getColor(R.color.white));
                fragmentProfileBinding.linearMale.setBackgroundResource(R.drawable.bg_linear_male_choose);

                fragmentProfileBinding.linearFemale.setBackgroundResource(R.drawable.bg_linear_female_no_choose);
                fragmentProfileBinding.imgFemale.setBackgroundResource(R.drawable.icon_female_choose_no);
                fragmentProfileBinding.txtFemale.setTextColor(getResources().getColor(R.color.colorTextSignup));

                fragmentProfileBinding.imgAvatar.setBackgroundResource(R.drawable.icon_avatar_male);
            }
        }
        fragmentProfileBinding.linearFemale.setOnClickListener(this);
        fragmentProfileBinding.linearMale.setOnClickListener(this);
        fragmentProfileBinding.toolbarSetting.imbBack.setOnClickListener(this);
        fragmentProfileBinding.txtSupport.setOnClickListener(this);
        fragmentProfileBinding.txtUpdate.setOnClickListener(this);
        fragmentProfileBinding.txtBirthday.setOnClickListener(this);
        fragmentProfileBinding.txtLogout.setOnClickListener(this);
        fragmentProfileBinding.txtNotifications.setOnClickListener(this);
    }

    private void updateBirthDay(Calendar ca) {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        fragmentProfileBinding.txtBirthday.setText(sdf.format(ca.getTime()));
    }

    private void call() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+84822251119"));
        startActivity(intent);
    }

    public  boolean isCallPhonePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(baseActivity, Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(baseActivity, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        }
        else {
            return true;
        }
    }

    private void updateInfor(final UpdateInforRequest mUpdateInfor) {
        mProgress = baseActivity.showLoading(baseActivity);
        subscription = apiServices.updateInfo(mUser.getId(), mUser.getToken(), mUpdateInfor.getAddress(),
                mUpdateInfor.getEmail(), mUpdateInfor.getFullname(), mUpdateInfor.getPhone(),
                mUpdateInfor.getGender(), mUpdateInfor.getDob())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(baseApplication.subscribeScheduler())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject o) {
                        if (o.get("status").getAsInt() != Constant.SUCCESS) {
                            baseActivity.hideLoading(mProgress);
                            JsonObject errorObject = o.get("error").getAsJsonObject();
                            baseActivity.alert(baseActivity, errorObject.get("msg").getAsString());
                        } else {
                            if (mUser != null) {
                                mRealm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        UserInfor userInfor = realm.where(UserInfor.class).equalTo("id", mUser.getId())
                                                .findFirst();
                                        userInfor.setFullname(mUpdateInfor.getFullname());
                                        userInfor.setAddress(mUpdateInfor.getAddress());
                                        userInfor.setEmail(mUpdateInfor.getEmail());
                                        userInfor.setPhone(mUpdateInfor.getPhone());
                                        userInfor.setGender(mUpdateInfor.getGender());
                                        userInfor.setDob(mUpdateInfor.getDob());
                                        initView();
                                        baseActivity.hideLoading(mProgress);
                                    }
                                });
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        baseActivity.hideLoading(mProgress);
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imb_back:
                baseActivity.finish();
                break;
            case R.id.txt_logout:
                if (mUser != null) {
                    mRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<UserInfor>  results = realm.where(UserInfor.class).equalTo("id", mUser.getId())
                                    .findAll();
                            results.clear();
                        }
                    });
                }
                baseActivity.saveUserId(baseActivity, "");
                baseActivity.saveToken(baseActivity, "");
                baseActivity.saveIsLogin(baseActivity, false);
                baseActivity.setResult(MainActivity.RESULT_CODE_SIGN_OUT);
                baseActivity.finish();
                break;
            case R.id.txt_support:
                if (isCallPhonePermissionGranted()) {
                    call();
                }
                break;
            case R.id.linear_female:
                fragmentProfileBinding.linearMale.setBackgroundResource(R.drawable.bg_linear_male_no_choose);
                fragmentProfileBinding.imgMale.setBackgroundResource(R.drawable.icon_male_choose_no);
                fragmentProfileBinding.txtMale.setTextColor(getResources().getColor(R.color.colorTextSignup));

                fragmentProfileBinding.linearFemale.setBackgroundResource(R.drawable.bg_linear_female_choose);
                fragmentProfileBinding.imgFemale.setBackgroundResource(R.drawable.icon_female_choose);
                fragmentProfileBinding.txtFemale.setTextColor(getResources().getColor(R.color.white));

                fragmentProfileBinding.linearMale.setBackgroundResource(R.drawable.bg_linear_male_no_choose);
                fragmentProfileBinding.imgAvatar.setBackgroundResource(R.drawable.icon_avatar_female);
                gender = "0";
                break;
            case R.id.linear_male:
                fragmentProfileBinding.imgMale.setBackgroundResource(R.drawable.icon_male_choose);
                fragmentProfileBinding.txtMale.setTextColor(getResources().getColor(R.color.white));
                fragmentProfileBinding.linearMale.setBackgroundResource(R.drawable.bg_linear_male_choose);

                fragmentProfileBinding.linearFemale.setBackgroundResource(R.drawable.bg_linear_female_no_choose);
                fragmentProfileBinding.imgFemale.setBackgroundResource(R.drawable.icon_female_choose);
                fragmentProfileBinding.txtFemale.setTextColor(getResources().getColor(R.color.colorTextSignup));

                fragmentProfileBinding.imgAvatar.setBackgroundResource(R.drawable.icon_avatar_male);
                gender = "1";
                break;
            case R.id.txt_birthday:
                new DatePickerDialog(baseActivity, date, mCalendar
                        .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                        mCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.txt_update:
                UpdateInforRequest updateInforRequest = new UpdateInforRequest();
                updateInforRequest.setEmail(fragmentProfileBinding.edtEmail.getText().toString());
                updateInforRequest.setAddress(fragmentProfileBinding.edtAdress.getText().toString());
                updateInforRequest.setPhone(fragmentProfileBinding.edtPhoneNumber.getText().toString());
                updateInforRequest.setDob(fragmentProfileBinding.txtBirthday.getText().toString());
                updateInforRequest.setGender(gender);
                updateInforRequest.setFullname(fragmentProfileBinding.edtFullName.getText().toString());
                if (!validate(fragmentProfileBinding.edtEmail.getText().toString())) {
                    if (mUser != null) {
                        if (!mUser.getId().equalsIgnoreCase("")) {
                            updateInfor(updateInforRequest);
                        }
                    }
                }
                break;
            case R.id.txt_notifications:
                ((SettingActivity) baseActivity).pushFragment(new FragmentListNotifications());
                break;

        }
    }

    private boolean validate(String mEmail) {
        if (!mEmail.equalsIgnoreCase("") && !android.util.Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
            baseActivity.alert(baseActivity, getResources().getString(R.string.validate_error_format_email));
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
            call();
        }
    }
}
