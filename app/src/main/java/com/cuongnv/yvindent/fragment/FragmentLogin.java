package com.cuongnv.yvindent.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.activity.MainActivity;
import com.cuongnv.yvindent.activity.RegisterActivity;
import com.cuongnv.yvindent.databinding.FragmentLoginBinding;
import com.cuongnv.yvindent.model.UserInfor;
import com.cuongnv.yvindent.network.ApiServices;
import com.cuongnv.yvindent.utils.BaseActivity;
import com.cuongnv.yvindent.utils.BaseApplication;
import com.cuongnv.yvindent.utils.BaseFragment;
import com.cuongnv.yvindent.utils.Constant;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by ITV01 on 1/11/17.
 */

public class FragmentLogin extends BaseFragment implements View.OnClickListener{
    private String TAG = FragmentLogin.class.getSimpleName();
    FragmentLoginBinding fragmentLoginBinding;
    BaseActivity baseActivity;
    BaseApplication baseApplication;
    ApiServices apiServices;
    Subscription subscription;
    ProgressDialog mProgress;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        baseActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (fragmentLoginBinding == null) {
            fragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
            initView();
        }
        return fragmentLoginBinding.getRoot();
    }

    private void initView() {
        baseApplication = BaseApplication.getInstance(baseActivity);
        apiServices = baseApplication.getApiServices();
        fragmentLoginBinding.btnSignUp.setOnClickListener(this);
        fragmentLoginBinding.btnLogin.setOnClickListener(this);
        mProgress = new ProgressDialog(baseActivity, R.style.AppCompatAlertDialogStyle);
    }

    private void login(String userName, String passWord, String os, String pushToken) {
        mProgress = baseActivity.showLoading(baseActivity);
        subscription = apiServices.login(userName, passWord, os, pushToken)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(baseApplication.subscribeScheduler())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject o) {
                        if (o.get("status").getAsInt() != Constant.SUCCESS) {
                            baseActivity.hideLoading(mProgress);
                            String error = o.get("msg").getAsString();
                            baseActivity.alert(baseActivity, error);
                            baseActivity.saveIsLogin(baseActivity, false);
                        } else {
                            JsonObject dataObject = o.get("data").getAsJsonObject();
                            JsonObject userObject = null;
                            baseActivity.saveIsLogin(baseActivity, true);
                            if (dataObject.has("User")) {
                                userObject = dataObject.getAsJsonObject("User");
                            }
                            UserInfor user = new UserInfor();
                            user = ((RegisterActivity) baseActivity).parseJsonUserInfor(userObject);
                            if (user != null) {
                                baseActivity.saveUserInfor(baseActivity, user);
                                baseActivity.saveUserId(baseActivity, user.getId());
                                baseActivity.saveToken(baseActivity, user.getToken());
                            }
                            baseActivity.hideLoading(mProgress);
                            if (baseActivity instanceof RegisterActivity) {
                                ((RegisterActivity) baseActivity).showActivity(MainActivity.class);
                            }

                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        baseActivity.hideLoading(mProgress);
                    }
                });
    }

    private boolean validate(String username, String password) {
        if (username.equalsIgnoreCase("")) {
            baseActivity.alert(baseActivity, getResources().getString(R.string.validate_username));
            return true;
        }
        if (password.equalsIgnoreCase("")) {
            baseActivity.alert(baseActivity, getResources().getString(R.string.validate_password));
            return true;
        }
        return false;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                baseActivity.hideKeyboard();
                if (!validate(fragmentLoginBinding.edtUsername.getText().toString(), fragmentLoginBinding.edtPassword.getText().toString())) {
                    String pushToken = baseActivity.getPushToken(baseActivity);
                    if (pushToken.equalsIgnoreCase("")) {
                        pushToken = FirebaseInstanceId.getInstance().getToken();
                    }
                    login(fragmentLoginBinding.edtUsername.getText().toString(), fragmentLoginBinding.edtPassword.getText().toString(),
                            "Android", pushToken);
                }
                break;
            case R.id.btn_sign_up:
                baseActivity.hideKeyboard();
                if (baseActivity instanceof RegisterActivity) {
                    ((RegisterActivity) baseActivity).showFragment(new FragmentRegister());
                }
                break;
        }
    }
}
