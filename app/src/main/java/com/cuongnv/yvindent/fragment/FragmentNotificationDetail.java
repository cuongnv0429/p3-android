package com.cuongnv.yvindent.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.databinding.FragmentNotificationDetailBinding;
import com.cuongnv.yvindent.model.Notification;
import com.cuongnv.yvindent.network.ApiServices;
import com.cuongnv.yvindent.utils.BaseActivity;
import com.cuongnv.yvindent.utils.BaseApplication;
import com.cuongnv.yvindent.utils.BaseFragment;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by ITV01 on 4/6/17.
 */

public class FragmentNotificationDetail extends BaseFragment{
    BaseActivity mActivity;
    FragmentNotificationDetailBinding mNotificationDetailBinding;
    BaseApplication mBaseApplication;
    ApiServices mApiServices;
    Subscription mSubscription;
    String id;
    public static FragmentNotificationDetail newInstance(String id) {
        FragmentNotificationDetail fragment = new FragmentNotificationDetail();
        Bundle args = new Bundle();
        args.putString("id", id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString("id");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mNotificationDetailBinding == null) {
            mNotificationDetailBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification_detail, container, false);
            initView();
        }
        return mNotificationDetailBinding.getRoot();
    }

    void initView() {
        mBaseApplication = BaseApplication.getInstance(mActivity);
        mApiServices = mBaseApplication.getApiServices();
        mNotificationDetailBinding.progressbarNotificationDetail.setVisibility(View.VISIBLE);
        mNotificationDetailBinding.linearNotificationDetail.setVisibility(View.GONE);
        if (!id.equalsIgnoreCase("")) {
            getListNotifications(id);
        }
        mNotificationDetailBinding.imbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
    }

    private void getListNotifications(String id) {
        mSubscription = mApiServices.getNotification(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(mBaseApplication.subscribeScheduler())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject o) {
                        mNotificationDetailBinding.progressbarNotificationDetail.setVisibility(View.GONE);
                        if (o.get("status").getAsInt() == 200) {
                            Notification mNotification = null;
                            if (o.getAsJsonObject("data") != null) {
                                mNotification = new Gson().fromJson(o.get("data"), Notification.class);
                            }
                            if (mNotification != null) {
                                mNotificationDetailBinding.linearNotificationDetail.setVisibility(View.VISIBLE);
                                mNotificationDetailBinding.txtTitle.setText(mNotification.getName());
                                mNotificationDetailBinding.webviewContent.getSettings().setJavaScriptEnabled(true);
                                mNotificationDetailBinding.webviewContent.loadDataWithBaseURL(null, mNotification.getDetail(), "text/html", "UTF-8", null);
                            }

                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mNotificationDetailBinding.progressbarNotificationDetail.setVisibility(View.GONE);
                    }
                });
    }
}
