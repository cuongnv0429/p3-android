package com.cuongnv.yvindent.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.activity.ReadPdfActivity;
import com.cuongnv.yvindent.adapter.CustomPagerAdapter;
import com.cuongnv.yvindent.adapter.ListPostAdapter;
import com.cuongnv.yvindent.adapter.RecyclerItemClickListener;
import com.cuongnv.yvindent.databinding.FragmentListPostBinding;
import com.cuongnv.yvindent.model.Ads;
import com.cuongnv.yvindent.model.Post;
import com.cuongnv.yvindent.network.ApiServices;
import com.cuongnv.yvindent.utils.BaseActivity;
import com.cuongnv.yvindent.utils.BaseApplication;
import com.cuongnv.yvindent.utils.BaseFragment;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by ITV01 on 1/16/17.
 */

public class FragmentListPost extends BaseFragment{
    private String TAG = FragmentListPost.class.getSimpleName();
    FragmentListPostBinding fragmentListPostBinding;
    BaseActivity baseActivity;
    BaseApplication baseApplication;
    ApiServices apiServices;
    Subscription subscription;
    private List<Post> postList;
    private List<Post> categoryPostList;
    private ListPostAdapter listPostAdapter;
    GridLayoutManager gridManager;
    int lastVisibleItem, totalItemCount = 0;
    private int visibleThreshold = 5;
    private boolean loading = false;
    private String userId = "";
    private String token = "";
    private int mPagerPosition;
    private Handler handler = new Handler();
    private int dotsCount;
    private ImageView[] dots;
    private CustomPagerAdapter customPagerAdapter;
    private List<Ads> adsList;
    private String category = "0";
    private boolean isLoad = true;
    private int currentPage = 1;
    Post curretnPost;

    private Runnable runnable = new Runnable() {
        public void run() {
            if (fragmentListPostBinding.viewpager.getAdapter() != null) {
                if (mPagerPosition >= fragmentListPostBinding.viewpager.getAdapter().getCount()) {
                    mPagerPosition = 0;
                } else {
                    mPagerPosition++;
                }
                try {
                    fragmentListPostBinding.viewpager.setCurrentItem(mPagerPosition, true);
                    handler.postDelayed(runnable, 5000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        baseActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (fragmentListPostBinding == null) {
            fragmentListPostBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_list_post, container, false);
            initView();
            getAds("", userId, token);
            initGrildView();
        }
        return fragmentListPostBinding.getRoot();
    }

    private void initView() {
        adsList = new ArrayList<>();
        baseApplication = BaseApplication.getInstance(baseActivity);
        apiServices = baseApplication.getApiServices();
        userId = baseActivity.getUserId(baseActivity);
        token = baseActivity.getToken(baseActivity);
        fragmentListPostBinding.swipeListPost.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loading = false;
                isLoad = true;
                getData(userId, token, 10, 1, category);
            }
        });
    }

    private void initGrildView() {
        postList = new ArrayList<>();
        categoryPostList = new ArrayList<>();
        listPostAdapter = new ListPostAdapter(baseActivity, postList);
        fragmentListPostBinding.recyclerListPost.setAdapter(listPostAdapter);
        gridManager = new GridLayoutManager(baseActivity, 2);
        fragmentListPostBinding.recyclerListPost.setLayoutManager(gridManager);
        fragmentListPostBinding.recyclerListPost.setHasFixedSize(true);
        fragmentListPostBinding.recyclerListPost.setItemViewCacheSize(20);
        fragmentListPostBinding.recyclerListPost.setDrawingCacheEnabled(true);
        fragmentListPostBinding.recyclerListPost.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        getData(userId, token, 10, 1, this.category);
        gridManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (listPostAdapter.isHeader(position)) {
                    return gridManager.getSpanCount();
                } else {
                    return listPostAdapter.getItemViewType(position);
                }

            }
        });
        fragmentListPostBinding.recyclerListPost.addOnItemTouchListener(
                new RecyclerItemClickListener(baseActivity, fragmentListPostBinding.recyclerListPost ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // do whatever
                        curretnPost = postList.get(position);
                        if (isStoragePermissionGranted()) {
                            Intent intent = new Intent(baseActivity, ReadPdfActivity.class);
                            intent.putExtra("postID", postList.get(position).getId());
                            startActivity(intent);
                        }
//                        baseActivity.alert(baseActivity, getResources().getString(R.string.text_demo_alert_pdf));
                    }
                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        fragmentListPostBinding.recyclerListPost.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {//check for scroll down
                    totalItemCount = gridManager.getItemCount();
                    lastVisibleItem = gridManager.findLastVisibleItemPosition();
                    if (!loading) {
                        if ((lastVisibleItem + visibleThreshold) >= totalItemCount) {
                            if (isLoad) {
                                loading = true;
                                postList.add(null);
                                listPostAdapter.notifyItemInserted(postList.size() - 1);
                                getData(userId, token, 10, currentPage, category);
                                currentPage++;
                            }
                        }
                    }
                }

            }
        });

    }

    public void resetData(String category) {
        currentPage = 1;
        this.category = category;
        postList.clear();
        listPostAdapter.notifyDataSetChanged();
        getData(userId, token, 10, currentPage, this.category);
    }

    private void getData(String userId, String token, final int limit, final int page, final String category) {
        if (page == 1 && !loading && !fragmentListPostBinding.swipeListPost.isRefreshing()) {
            fragmentListPostBinding.progressBarListPost.setVisibility(View.VISIBLE);
            fragmentListPostBinding.recyclerListPost.setVisibility(View.GONE);
        }
        subscription = apiServices.getData(userId, token,
                limit, page, "Android")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(baseApplication.subscribeScheduler())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject o) {
                        if (o.get("status").getAsInt() == 200) {
                            if (fragmentListPostBinding.swipeListPost.isRefreshing()) {
                                isLoad = true;
                                loading = false;
                                currentPage = 1;
                            }
                            List<Post> list = new ArrayList<Post>();
                            JsonArray dataArray = o.getAsJsonArray("data");
                            if (dataArray.size() < 10) {
                                isLoad = false;
                            } else {
                                isLoad = true;
                            }
                            for (int i = 0; i < dataArray.size(); i++) {
                                JsonObject json = dataArray.get(i).getAsJsonObject();
                                JsonObject postObject = json.getAsJsonObject("Post");
                                Post post = parseJson(postObject);
                                Log.e(TAG, post.getId() );
                                if (category.equalsIgnoreCase("0")) {
                                    list.add(post);
                                } else if (post.getCategories().equalsIgnoreCase(category)) {
                                    list.add(post);
                                }
                            }
                            if (fragmentListPostBinding.swipeListPost.isRefreshing()) {
                                postList.clear();
                            }
                            if (loading == true) {
                                loading = false;
                                postList.remove(postList.size() - 1);
                                listPostAdapter.notifyItemRemoved(postList.size());
                            }
                            if (list.size() > 0) {
                                postList.addAll(list);
                                listPostAdapter.notifyItemInserted(postList.size());
                            }
                        }
                        fragmentListPostBinding.progressBarListPost.setVisibility(View.GONE);
                        fragmentListPostBinding.swipeListPost.setRefreshing(false);
                        if (postList.size() > 0) {
                            fragmentListPostBinding.swipeListPost.setVisibility(View.VISIBLE);
                            fragmentListPostBinding.recyclerListPost.setVisibility(View.VISIBLE);
                            fragmentListPostBinding.txtAlertNoData.setVisibility(View.GONE);
                        } else {
                            fragmentListPostBinding.swipeListPost.setVisibility(View.GONE);
                            fragmentListPostBinding.recyclerListPost.setVisibility(View.GONE);
                            fragmentListPostBinding.txtAlertNoData.setVisibility(View.VISIBLE);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        fragmentListPostBinding.progressBarListPost.setVisibility(View.GONE);
                        fragmentListPostBinding.swipeListPost.setRefreshing(false);
                    }
                });
    }

    private void getAds(String postID, String userId, String token) {
        subscription = apiServices.ads(postID, userId, token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(baseApplication.subscribeScheduler())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject o) {
                        if (o.get("status").getAsInt() == 200) {
                            fragmentListPostBinding.relative.setVisibility(View.VISIBLE);
                            JsonArray dataArray = o.getAsJsonArray("data");
                            for (int i = 0; i < dataArray.size(); i++) {
                                JsonObject json = dataArray.get(i).getAsJsonObject();
                                Ads ads= parseJsonAds(json);
                                adsList.add(ads);
                            }
                            if (adsList.size() > 1) {
                                setupViewpager();
                                handler.postDelayed(runnable, 2000);
                            }
                        } else {
                            fragmentListPostBinding.relative.setVisibility(View.GONE);
                        }
                        fragmentListPostBinding.progressBarListPost.setVisibility(View.GONE);
                        fragmentListPostBinding.recyclerListPost.setVisibility(View.VISIBLE);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        fragmentListPostBinding.progressBarListPost.setVisibility(View.GONE);
                    }
                });
    }

    private Ads parseJsonAds(JsonObject json) {
        Ads ads = new Ads();
        if (json.has("id")) {
            ads.setId(json.get("id").getAsString());
        }
        if (json.has("title")) {
            ads.setTitle(json.get("title").getAsString());
        }
        if (json.has("img")) {
            ads.setImg(json.get("img").getAsString());
        }
        if (json.has("urlRedirect")) {
            ads.setUrlRedirect(json.get("urlRedirect").getAsString());
        }
        if (json.has("stt")) {
            ads.setStt(json.get("stt").getAsString());
        }

        return ads;
    }

    private Post parseJson(JsonObject json) {
        Post post = new Post();
        if (json.has("id")) {
            post.setId(json.get("id").getAsString());
        }
        if (json.has("title")) {
            post.setTitle(json.get("title").getAsString());
        }
        if (json.has("date")) {
            post.setDate(json.get("date").getAsString());
        }
        if (json.has("thumbImg")) {
            post.setThumbImg(json.get("thumbImg").getAsString());
        }
        if (json.has("urlDemo")) {
            post.setUrlDemo(json.get("urlDemo").getAsString());
        }
        if (json.has("description")) {
            post.setDescription(json.get("description").getAsString());
        }
        if (json.has("stt")) {
            post.setStt(json.get("stt").getAsString());
        }
        if (json.has("categories")) {
            post.setCategories(json.get("categories").getAsString());
        }
        return post;
    }

    private void setupViewpager() {
        customPagerAdapter = new CustomPagerAdapter(baseActivity, adsList);
        fragmentListPostBinding.viewpager.setAdapter(customPagerAdapter);
        if (adsList.size() > 1) {
            setUiPageViewController();
        }
        fragmentListPostBinding.viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mPagerPosition = position;
                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(getResources().getDrawable(R.drawable.dots_normal));
                }
                dots[position].setImageDrawable(getResources().getDrawable(R.drawable.dots_active));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        fragmentListPostBinding.viewpager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                Log.e(TAG, "setOnTouchListener");
//                if (handler != null) {
//                    handler.removeCallbacks(runnable);
//                }
                return false;
            }
        });

//        fragmentListPostBinding.viewpager.setOnItemClickListener(new ClickableViewPager.OnItemClickListener() {
//            @Override
//            public void onItemClick(int position) {
//                Log.e(TAG, "setOnItemClickListener");
//                if (!adsList.get(position).getUrlRedirect().equalsIgnoreCase("")) {
//                    Intent intent = new Intent(baseActivity, ViewAdsActivity.class);
//                    intent.putExtra("url", adsList.get(position).getUrlRedirect());
//                    startActivity(intent);
//                }
//            }
//        });
    }

    private void setUiPageViewController() {
        dotsCount = customPagerAdapter.getCount();
        dots = new ImageView[dotsCount];
        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(baseActivity);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.dots_normal));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(4, 0, 4, 0);
            fragmentListPostBinding.linearDot.addView(dots[i], params);
        }
        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.dots_active));
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(baseActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(baseActivity,
                    Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {
                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(baseActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
                Intent intent = new Intent(baseActivity, ReadPdfActivity.class);
                intent.putExtra("postID", curretnPost.getId());
                startActivity(intent);
                //resume tasks needing this permission
            }
        }
    }
}
