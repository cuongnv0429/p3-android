package com.cuongnv.yvindent.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.cuongnv.yvindent.R;
import com.cuongnv.yvindent.activity.MainActivity;
import com.cuongnv.yvindent.activity.RegisterActivity;
import com.cuongnv.yvindent.databinding.FragmentRegisterBinding;
import com.cuongnv.yvindent.model.RegisterRequest;
import com.cuongnv.yvindent.model.UserInfor;
import com.cuongnv.yvindent.network.ApiServices;
import com.cuongnv.yvindent.utils.BaseActivity;
import com.cuongnv.yvindent.utils.BaseApplication;
import com.cuongnv.yvindent.utils.BaseFragment;
import com.cuongnv.yvindent.utils.Constant;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by ITV01 on 1/14/17.
 */

public class FragmentRegister extends BaseFragment implements View.OnClickListener{
    private String TAG = FragmentLogin.class.getSimpleName();
    FragmentRegisterBinding fragmentRegisterBinding;
    BaseActivity baseActivity;
    BaseApplication baseApplication;
    ApiServices apiServices;
    Subscription subscription;
    private ProgressDialog mProgress;
    private ProgressDialog mPopup;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        baseActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (fragmentRegisterBinding == null) {
            fragmentRegisterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);
            initView();
        }
        return fragmentRegisterBinding.getRoot();
    }

    private void initView() {
        baseApplication = BaseApplication.getInstance(baseActivity);
        apiServices = baseApplication.getApiServices();
        fragmentRegisterBinding.btnCancel.setOnClickListener(this);
        fragmentRegisterBinding.btnRegister.setOnClickListener(this);
        fragmentRegisterBinding.btnForgotPassword.setOnClickListener(this);
        mProgress = new ProgressDialog(baseActivity, R.style.AppCompatAlertDialogStyle);
    }

    private void register(RegisterRequest registerRequest) {
        mProgress = baseActivity.showLoading(baseActivity);
        String pushToken = baseActivity.getPushToken(baseActivity);
        if (pushToken.equalsIgnoreCase("")) {
            pushToken = FirebaseInstanceId.getInstance().getToken();
        }
        subscription = apiServices.register(registerRequest.getUsername(), registerRequest.getEmail(),
                registerRequest.getPassword(), registerRequest.getUsername(), pushToken, "Android")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(baseApplication.subscribeScheduler())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject o) {
                        if (o.get("status").getAsInt() != Constant.SUCCESS) {
                            baseActivity.hideLoading(mProgress);
                            JsonObject errorObject = o.get("error").getAsJsonObject();
                            baseActivity.alert(baseActivity, errorObject.get("msg").getAsString());
                        } else {
                            JsonObject dataObject = o.get("data").getAsJsonObject();
                            JsonObject userObject = null;
                            if (dataObject.has("User")) {
                                userObject = dataObject.getAsJsonObject("User");
                            }
                            UserInfor user = ((RegisterActivity) baseActivity).parseJsonUserInfor(userObject);
                            if (user != null) {
                                baseActivity.saveUserInfor(baseActivity, user);
                                baseActivity.saveUserId(baseActivity, user.getId());
                                baseActivity.saveToken(baseActivity, user.getToken());
                            }
                            Log.e(TAG, baseActivity.getToken(baseActivity));
                            baseActivity.hideLoading(mProgress);
                            if (baseActivity instanceof RegisterActivity) {
                                ((RegisterActivity) baseActivity).showActivity(MainActivity.class);
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        baseActivity.hideLoading(mProgress);
                    }
                });
    }

    private void forgotPassword(String mEmail) {
        mProgress = baseActivity.showLoading(baseActivity);
        subscription = apiServices.forgotPassword(mEmail)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(baseApplication.subscribeScheduler())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject o) {
                        baseActivity.hideLoading(mProgress);
                        if (o.get("status").getAsInt() == Constant.SUCCESS) {
                            mPopup.dismiss();
                            String error = o.get("msg").getAsString();
                            baseActivity.alert(baseActivity, error);
                        } else {
                            String error = o.get("error").getAsString();
                            baseActivity.alert(baseActivity, error);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        baseActivity.hideLoading(mProgress);
                    }
                });
    }

    private boolean validate(RegisterRequest registerRequest) {
        if (registerRequest.getPassword().equalsIgnoreCase("")) {
            baseActivity.alert(baseActivity, getResources().getString(R.string.validate_password));
            return true;
        }
        if (fragmentRegisterBinding.edtConfirmPassword.getText().toString().equalsIgnoreCase("")) {
            baseActivity.alert(baseActivity, getResources().getString(R.string.validate_confirm_password));
            return true;
        }
        if (registerRequest.getEmail().equalsIgnoreCase("")) {
            baseActivity.alert(baseActivity, getResources().getString(R.string.validate_email));
            return true;
        }
        if (registerRequest.getUsername().equalsIgnoreCase("")) {
            baseActivity.alert(baseActivity, getResources().getString(R.string.validate_username));
            return true;
        }

        if (!registerRequest.getPassword().equalsIgnoreCase(fragmentRegisterBinding.edtConfirmPassword.getText().toString())) {
            baseActivity.alert(baseActivity, getResources().getString(R.string.validate_password_no_same_confirm_password));
            return true;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(registerRequest.getEmail()).matches()) {
            baseActivity.alert(baseActivity, getResources().getString(R.string.validate_error_format_email));
            return true;
        }
        return false;
    }

    public ProgressDialog showPopupForgotPassword(Context context) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, null, null, true);
        progressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        progressDialog.setContentView(R.layout.dialog_forgot_password);
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        TextView txtOk = (TextView) progressDialog.findViewById(R.id.txt_ok);
        final TextView edtEmail = (EditText) progressDialog.findViewById(R.id.edt_email);
        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtEmail.getText().toString().equalsIgnoreCase("")) {
                    baseActivity.alert(baseActivity, getResources().getString(R.string.validate_email));
                } else {
                    forgotPassword(edtEmail.getText().toString());
                }
            }
        });

        TextView txtCancel = (TextView) progressDialog.findViewById(R.id.txt_cancel);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.dismiss();
            }
        });
        return progressDialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register:
                RegisterRequest registerRequest = new RegisterRequest();
                registerRequest.setPassword(fragmentRegisterBinding.edtPassword.getText().toString());
                registerRequest.setEmail(fragmentRegisterBinding.edtEmail.getText().toString());
                registerRequest.setUsername(fragmentRegisterBinding.edtUserName.getText().toString());
                if (!validate(registerRequest)) {
                    baseActivity.hideKeyboard();
                    register(registerRequest);
                }
                break;
            case R.id.btn_cancel:
                baseActivity.onBackPressed();
                break;
            case R.id.btn_forgot_password:
                mPopup = showPopupForgotPassword(baseActivity);
                mPopup.show();
                break;
        }
    }
}
