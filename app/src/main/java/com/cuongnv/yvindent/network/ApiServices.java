package com.cuongnv.yvindent.network;

import com.google.gson.JsonObject;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by ITV01 on 1/12/17.
 */

public interface ApiServices {
    @FormUrlEncoded
    @POST("register")
    Observable<JsonObject> register(@Field("fullname") String fullname, @Field("email") String email,
                                    @Field("password") String password, @Field("username") String username,
                                    @Field("os") String os, @Field("token") String token);

    @FormUrlEncoded
    @POST("login")
    Observable<JsonObject> login(@Field("username") String username, @Field("password") String password,
                                 @Field("os") String os, @Field("token") String token);

    @GET("home")
    Observable<JsonObject> getData(@Query("userID") String userID, @Query("token") String token, @Query("limit") int limit,
                                   @Query("page") int page, @Query("os") String os);

    @FormUrlEncoded
    @POST("postread")
    Observable<JsonObject> readPdf(@Field("postID") String postID, @Field("userID") String userID,
                                   @Field("token") String token);

    @FormUrlEncoded
    @POST("ads")
    Observable<JsonObject> ads(@Field("postID") String postID, @Field("userID") String userID,
                                    @Field("token") String token);

    @FormUrlEncoded
    @POST("search")
    Observable<JsonObject> search(@Field("keyword") String keyword, @Field("cat") String cat);

    @FormUrlEncoded
    @POST("forgotpass")
    Observable<JsonObject> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("updateinfo")
    Observable<JsonObject> updateInfo(@Field("userID") String userID, @Field("token") String token,
                                      @Field("address") String address, @Field("email") String email,
                                      @Field("fullname") String fullname,
                                      @Field("phone") String phone,
                                      @Field("gender") String gender,
                                      @Field("dob") String dob);
    @POST("notice")
    Observable<JsonObject> getListNotifications();

    @FormUrlEncoded
    @POST("getnotice")
    Observable<JsonObject> getNotification(@Field("id") String id);
}
